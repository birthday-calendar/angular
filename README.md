# BirthdayCalendar Angular

Данное приложение выводит дни рождения, полученные из [BirthdayCalendar API](https://gitlab.com/birthday-calendar/api).

## Для запуска

Перед запуском следует отредактировать person.service.ts (src/app/shared/services)

Измените литерал переменной URL на 10 строке на URL запущенного API

```typescript
URL: string = 'https://localhost:44301/api';
```