import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ShowComponent} from "./pages/showPage/show.component";
import {AddComponent} from "./pages/addPage/add.component";
import {EditComponent} from "./pages/editPage/edit.component";

const routes: Routes = [
  {path: 'create', component: AddComponent},
  {path: 'edit/:id', component: EditComponent},
  {path: '', component: ShowComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
