import { Component, OnInit } from '@angular/core';
import {PersonService} from "../../shared/services/person.service";
import {PersonAdd} from "../../shared/entities/person-add";

@Component({
  selector: 'add-person',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css'],
  providers: [
    PersonService
  ]
})
export class AddComponent implements OnInit {
  name: string = "";
  date: string = "";
  file: File | null = null ;

  constructor(private service: PersonService) { }

  ngOnInit(): void {
  }

  onFileChange(file: FileList) {
    this.file = file.item(0);
  }

  sendData() {
    if (this.file)
    this.service.addPerson(new PersonAdd(this.name, new Date(this.date), this.file!)).subscribe();
    else console.error('file is undefined');
  }
}
