import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddComponent } from './add.component';
import {FormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";



@NgModule({
  declarations: [
    AddComponent
  ],
  imports: [
    FormsModule,
    BrowserModule
  ]
})
export class AddModule { }
