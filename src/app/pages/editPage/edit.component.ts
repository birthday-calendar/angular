import { Component, OnInit } from '@angular/core';
import {PersonService} from "../../shared/services/person.service";
import {ActivatedRoute} from "@angular/router";
import {PersonAdd} from "../../shared/entities/person-add";
import {PersonUpdate} from "../../shared/entities/person-update";

@Component({
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css'],
  providers: [
    PersonService
  ]
})
export class EditComponent implements OnInit {
  id: number | undefined;
  name: string = "";
  date: string = "";
  file: File | null = null;

  constructor(private service: PersonService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => this.id = params['id']);
  }

  onFileChange(file: FileList) {
    this.file = file.item(0);
  }

  sendData() {
    console.log("send data");
    if (this.file)
      this.service.updatePerson(new PersonUpdate(this.id!, this.name, new Date(this.date), this.file!)).subscribe((data: any) => console.log(data));
    else console.error('file is undefined');
  }

  loadData() {
    let person = this.service.getPerson(this.id!).subscribe(
      person => {
        this.name = person.name;
      }
    )
  }
}
