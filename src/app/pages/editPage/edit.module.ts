import { NgModule } from '@angular/core';
import { EditComponent } from './edit.component';
import {FormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";



@NgModule({
  declarations: [
    EditComponent
  ],
  imports: [
    FormsModule,
    BrowserModule
  ]
})
export class EditModule { }
