import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Person} from "../../../../shared/entities/person";
import {PersonService} from "../../../../shared/services/person.service";

@Component({
  selector: 'person-element',
  templateUrl: './person-element.component.html',
  styleUrls: ['./person-element.component.css'],
  providers: [
    PersonService
  ]
})
export class PersonElementComponent implements OnInit {
  @Input() person: Person =
    new Person(0, '', '', false, false, false);
  @Output() onChanged = new EventEmitter<boolean>();
  photo: string | null = null;

  constructor(private service: PersonService) { }

  ngOnInit(): void {
    if (this.person == undefined) console.log("Person is undefined")
    this.loadPhoto(this.person!.id)
  }

  private loadPhoto(personId: number) {
    this.service.getPhoto(personId).subscribe(photo => this.createImageFromBlob(photo));
  }

  createImageFromBlob(image: Blob) {
    let reader = new FileReader();
    reader.addEventListener("load", () => {
      this.photo = reader.result as string;
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  removePerson(id: number) {
    this.service.deleteProduct(id).subscribe(data => this.onChanged.emit());
  }
}
