import { Component, OnInit } from '@angular/core';
import {PersonService} from "../../shared/services/person.service";
import {Person} from "../../shared/entities/person";

@Component({
  selector: 'show-birthdays',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css'],
  providers: [
    PersonService
  ]
})
export class ShowComponent implements OnInit {
  persons: Person[] = [];
  nearest: boolean = false;
  showList: boolean = true;

  constructor(private service: PersonService) { }

  ngOnInit(): void {
    this.updatePersonsList();
  }

  updatePersonsList() {
    this.showList = false;
    this.service.getPersons(this.nearest).subscribe(p => {
      this.persons = p;
      this.showList = true;
      console.log(this.persons);
      console.log(this.nearest);
    });
  }

}
