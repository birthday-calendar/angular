import { NgModule } from '@angular/core';
import {BrowserModule} from "@angular/platform-browser";
import { ShowComponent } from './show.component';
import { PersonElementComponent } from './child-components/person-element/person-element.component';
import {FormsModule} from "@angular/forms";
import {RouterLink} from "@angular/router";



@NgModule({
  declarations: [
    ShowComponent,
    PersonElementComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterLink
  ]
})
export class ShowModule { }
