export class PersonAdd {
  constructor(
    public name : string,
    public birthday : Date,
    public photo : File
  ) {
  }
}
