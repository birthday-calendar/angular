export class PersonUpdate {
  constructor(
    public id : number,
    public name : string,
    public birthday : Date,
    public photo : File
  ) {
  }
}
