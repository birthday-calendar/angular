export class Person {
  constructor(
    public id : number,
    public name : string,
    public birthday : string,
    public isNear : boolean,
    public isToday : boolean,
    public isPast : boolean
  ) {
  }
}
