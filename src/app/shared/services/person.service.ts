import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {Person} from "../entities/person";
import {PersonAdd} from "../entities/person-add";
import {PersonUpdate} from "../entities/person-update";

@Injectable()
export class PersonService {
  URL : string = 'https://localhost:44301/api';

  constructor(private http : HttpClient) { }

  public getPersons(nearest : boolean) : Observable<Person[]> {
    return this.http.get(`${this.URL}/persons?nearest=${nearest ? 'true' : 'false'}`).pipe(map((data: any) => {
      return data as Person[];
    }))
  }

  public getPerson(id: number) : Observable<Person> {
    return this.http.get(`${this.URL}/persons/${id}`).pipe(map(
      (data: any) => {
        return data as Person;
      }
    ))
  }

  public getPhoto(id : number) : Observable<Blob> {
    return this.http.get(`${this.URL}/persons/${id}/photo`, { responseType: 'blob' });
  }

  public addPerson(person: PersonAdd) : Observable<any> {
    let form = new FormData();
    let birthdayString = person.birthday.toISOString();
    console.log(birthdayString);
    form.append('name', person.name);
    form.append('birthday', person.birthday.toISOString());
    form.append('photo', person.photo);
    return this.http.post(`${this.URL}/persons`, form);
  }

  public updatePerson(person: PersonUpdate) : Observable<any> {
    console.log("update person");
    let form = new FormData();
    let birthdayString = person.birthday.toISOString();
    form.append('id', person.id.toString());
    form.append('name', person.name);
    form.append('birthday', birthdayString);
    form.append('photo', person.photo);
    return this.http.put(`${this.URL}/persons`, form);
  }

  public deleteProduct(id: number) : Observable<any> {
    return this.http.delete(`${this.URL}/persons/${id}`);
  }
}
